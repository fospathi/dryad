package dryad

import (
	"errors"
	"sync"

	"slices"
)

// A SyncAwaitSeq is a concurrency safe awaitable sequence of values.
//
// An Await shall be closed before using it to await the next position or
// getting the value at that position.
type SyncAwaitSeq[T any] struct {
	// The Done channel is closed by the Close() method.
	Done <-chan struct{}
	done chan struct{}

	vv []T
	vc []chan struct{}
	m  sync.Mutex
}

// Advance to the next position in the sequence after the given Await position.
//
// Supply a nil argument to await the first position in the sequence. Otherwise,
// to await a position, you need to supply the already closed Await of the
// preceding position.
//
// If the returned Await is not already closed, the position of the Await is as
// yet unfilled.
//
// An Await closes upon a value being appended to the sequence. An Await also
// closes upon closure of the sequence leaving the last unfilled position
// forever unfilled.
//
// Return an error if no such Await exists or the position is not filled. You
// cannot advance past the unfilled last position.
func (seq *SyncAwaitSeq[T]) Advance(await Await) (Await, error) {
	seq.m.Lock()
	defer seq.m.Unlock()

	if await == nil {
		return seq.vc[0], nil
	}
	l := len(seq.vc)
	if await == seq.vc[l-1] {
		return nil, errUnfilled
	}
	for i := l - 2; i >= 0; i-- {
		if await == seq.vc[i] {
			return seq.vc[i+1], nil
		}
	}
	return nil, errAwaitNotFound
}

// Append the given value to the end of the sequence.
func (seq *SyncAwaitSeq[T]) Append(v T) {
	seq.m.Lock()
	defer seq.m.Unlock()

	select {
	case <-seq.done:
		return
	default:
	}
	seq.vv = append(seq.vv, v)
	seq.vc = append(seq.vc, make(chan struct{}))
	close(seq.vc[len(seq.vc)-2])
}

// AppendAll the given values to the end of the sequence.
func (seq *SyncAwaitSeq[T]) AppendAll(vv []T) {
	seq.m.Lock()
	defer seq.m.Unlock()

	select {
	case <-seq.done:
		return
	default:
	}
	for _, v := range vv {
		seq.vv = append(seq.vv, v)
		seq.vc = append(seq.vc, make(chan struct{}))
		close(seq.vc[len(seq.vc)-2])
	}
}

// Await is a synonym for [Advance].
func (seq *SyncAwaitSeq[T]) Await(await Await) (Await, error) {
	return seq.Advance(await)
}

// Close the sequence.
//
// No more values can be added afterwards.
func (seq *SyncAwaitSeq[T]) Close() {
	seq.m.Lock()
	defer seq.m.Unlock()

	select {
	case <-seq.done:
		return
	default:
	}

	// Close done before closing the last unfilled await to accommodate the end
	// user usage pattern of:
	//
	//     select {
	//     case <-awaitChannel:
	//         select {
	//         case <-sequence.Done:
	//             ...
	//             return
	//         default:
	//         }
	//         ...
	//     }
	//
	// Closing the await first could mean done is not closed soon enough to
	// select the done case.
	close(seq.done)
	close(seq.vc[len(seq.vc)-1])
}

// Delete the position in the sequence associated with the given Await, reducing
// the length of the sequence by 1.
//
// Return the value occupying that position before deletion.
//
// Return an error if no such Await exists or the position is not filled.
func (seq *SyncAwaitSeq[T]) Delete(await Await) (T, error) {
	seq.m.Lock()
	defer seq.m.Unlock()

	var (
		l     = len(seq.vc)
		zeroT T
	)
	if await == seq.vc[l-1] {
		return zeroT, errUnfilled
	}
	removeAt := func(i int) T {
		v := seq.vv[i]
		seq.vv = slices.Delete(seq.vv, i, i+1)
		seq.vc = slices.Delete(seq.vc, i, i+1)
		return v
	}
	for i := 0; i < l-1; i++ {
		if await == seq.vc[i] {
			return removeAt(i), nil
		}
	}
	return zeroT, errAwaitNotFound
}

// Length of the sequence.
func (seq *SyncAwaitSeq[T]) Length() int {
	seq.m.Lock()
	defer seq.m.Unlock()

	return len(seq.vv)
}

// Value in the sequence at the given awaited position.
//
// Return an error if no such Await exists or the position is not filled.
func (seq *SyncAwaitSeq[T]) Value(await Await) (T, error) {
	seq.m.Lock()
	defer seq.m.Unlock()

	var (
		l     = len(seq.vc)
		zeroT T
	)
	if await == seq.vc[l-1] {
		return zeroT, errUnfilled
	}
	for i := l - 2; i >= 0; i-- {
		if await == seq.vc[i] {
			return seq.vv[i], nil
		}
	}
	return zeroT, errAwaitNotFound
}

// Values of the sequence as a new slice.
func (seq *SyncAwaitSeq[T]) Values() []T {
	seq.m.Lock()
	defer seq.m.Unlock()

	return slices.Clone(seq.vv)
}

// NewSyncAwaitSeq constructs a new SyncAwaitSeq.
func NewSyncAwaitSeq[T any]() *SyncAwaitSeq[T] {
	done := make(chan struct{})
	seq := &SyncAwaitSeq[T]{
		done: done,
		Done: done,
		vc:   []chan struct{}{make(chan struct{})},
	}
	return seq
}

// IsUnfilledError reports whether the given error indicates that an Await which
// was supplied as an argument does not have an associated value.
//
// Perhaps the Await is not yet closed. Or perhaps the Await is closed but is
// not associated with a value, as can happen for example to the final Await of
// a closed awaitable sequence.
func IsUnfilledError(err error) bool {
	return errors.Is(err, errUnfilled)
}

var errUnfilled error = errors.New("the await position is unfilled")

// IsAwaitNotFoundError reports whether the given error indicates that an Await
// which was supplied as an argument was not found among the current awaits.
//
// Perhaps the Await in question was deleted.
func IsAwaitNotFoundError(err error) bool {
	return errors.Is(err, errAwaitNotFound)
}

var errAwaitNotFound error = errors.New("no such await was found")
