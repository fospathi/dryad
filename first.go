package dryad

// NewFirst constructs a new First channel which is a channel that is preloaded
// with one value.
//
// Only the first receiver gets the value. Once the channel is emptied no
// further values can be added, the channel can never be closed, and any other
// channel read operations will block.
//
// One possible use of a First is as a conditional 'once':
//
//	select {
//	case <-first:
//	default:
//	   // Perform these actions more than once for non first read operations...
//	   return
//	}
//	// Perform the following actions only once...
func NewFirst[T any](v T) <-chan T {
	first := make(chan T, 1)
	first <- v
	return first
}
