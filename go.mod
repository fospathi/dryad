module gitlab.com/fospathi/dryad

go 1.21

require golang.org/x/exp v0.0.0-20231006140011-7918f672742d

replace gitlab.com/fospathi/maf => ../maf

replace gitlab.com/fospathi/universal => ../universal
