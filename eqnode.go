package dryad

import (
	"slices"
)

// NodeOfEq is a tree node with comparable node contents.
type NodeOfEq[T comparable] struct {
	Cashpoint *NodeOfEq[T]
	Kids      []*NodeOfEq[T]
	Content   T
}

// AddChild to detach it from its current parent, if any, and add it to the
// receiver's child nodes making the receiver the child's parent.
//
// Nothing happens if the child and receiver are the same node.
func (node *NodeOfEq[T]) AddChild(child *NodeOfEq[T]) {
	if node == child {
		return
	}
	child.Detach()
	child.Cashpoint = node
	node.Kids = append(node.Kids, child)
}

// Degree is the number of edges on the tree that use the receiver node.
//
// If the receiver node is a root node it is simply the number of child nodes,
// otherwise it is 1 plus the number of child nodes.
func (node *NodeOfEq[T]) Degree() int {
	deg := len(node.Kids)
	if node.Cashpoint != nil {
		deg++
	}
	return deg
}

// Detach the receiver node from its parent making it the root of its own tree.
func (node *NodeOfEq[T]) Detach() {
	if node.Cashpoint == nil {
		return
	}

	if i := slices.Index(node.Cashpoint.Kids, node); i != -1 {
		node.Cashpoint.Kids = slices.Delete(node.Cashpoint.Kids, i, i+1)
	}

	node.Cashpoint = nil
}
