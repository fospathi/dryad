package dryad

type Geter[T any] interface {
	// Get the value.
	Get() T
}

// finalised is set once at creation.
type finalised[T any] struct {
	val T
}

func (f *finalised[T]) Get() T {
	return f.val
}

// NewFinalised constructs a new Finalised permanently set to the given value.
func NewFinalised[T any](val T) Geter[T] {
	return &finalised[T]{
		val: val,
	}
}

// Final can only be set once.
type Final[T any] struct {
	set bool
	val T
}

// Get the value if set else the zero value of its type.
func (f *Final[T]) Get() T {
	return f.val
}

// Set the value.
//
// Has no effect after the first use.
func (f *Final[T]) Set(val T) {
	if f.set {
		return
	}
	f.set = true
	f.val = val
}

// Value stored in the final and a true if set, else the zero value of the type
// and a false to indicate it is unset.
func (f *Final[T]) Value() (val T, ok bool) {
	val, ok = f.val, f.set
	return
}
