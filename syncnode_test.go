package dryad_test

import (
	"testing"

	"gitlab.com/fospathi/dryad"
)

func TestSyncNode(t *testing.T) {

	//
	// Test the methods of an empty node.
	//

	node := &dryad.SyncNode[string]{}

	// Test the degree method.

	{
		want := 0
		got := node.Degree()
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	// Test the parent and children methods.

	node.Detach()

	{
		want1, want2 :=
			(*dryad.SyncNode[string])(nil), ([](*dryad.SyncNode[string]))(nil)
		got1, got2 := node.Parent(), node.Children()

		if want1 != got1 || nil != got2 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n",
				want1, got1, want2, got2)
		}
	}

	// Test the value methods.

	{
		want := ""
		got := node.Get()
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}

		msg := "hello, world!"
		node.Set(msg)

		want = msg
		got = node.Get()
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	//
	// Test adding and removing nodes.
	//

	nodeA := &dryad.SyncNode[string]{}
	nodeB := &dryad.SyncNode[string]{}
	nodeC := &dryad.SyncNode[string]{}
	nodeD := &dryad.SyncNode[string]{}

	nodeA.Set("A")
	nodeB.Set("B")
	nodeC.Set("C")
	nodeD.Set("D")

	nodeA.AddChild(nodeB)

	{
		want1, want2 := nodeA.Degree(), nodeB.Degree()
		got1, got2 := 1, 1

		if want1 != got1 || want2 != got2 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n",
				want1, got1, want2, got2)
		}
	}

	{
		want := true
		got := nodeB.Parent() == nodeA && nodeA.Children()[0] == nodeB &&
			nodeB.Parent().Get() == "A"
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	// Test more than one child.

	nodeA.AddChild(nodeC)

	{
		want1, want2 := nodeA.Degree(), nodeC.Degree()
		got1, got2 := 2, 1

		if want1 != got1 || want2 != got2 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n",
				want1, got1, want2, got2)
		}

		nodeC.AddChild(nodeD)

		want := true
		got := nodeC.Degree() == 2 && nodeD.Parent().Parent().Get() == "A"
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	// Test a detaching addChild.

	nodeB.AddChild(nodeD)

	{
		want := true
		got := nodeD.Parent().Get() == "B" &&
			len(nodeC.Children()) == 0 &&
			len(nodeB.Children()) == 1
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	nodeB.AddChild(nodeD) // Re add an already present child.
	nodeB.AddChild(nodeB) // Add itself as child.

	{
		want := true
		got := nodeB.Children()[0].Get() == "D" &&
			len(nodeB.Children()) == 1
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}
}
