/* Package dryad provides generic data structures. */
package dryad

// Await something.
//
// The waiting is finished when the Await is closed. Use the closed Await to
// access the something that was awaited.
type Await <-chan struct{}

// Identity function returns its argument.
func Identity[T any](v T) T {
	return v
}
