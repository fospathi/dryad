package modslice

// CanonShift converts a shift value n, which can be positive or negative and
// greater in magnitude than the slice length l, to a canonical shift value
// which is positive and less than the slice length.
//
// Special cases:
//   - for an empty slice the canonical shift value is zero.
func CanonShift(l, n int) int {
	if l == 0 {
		return 0
	}
	n %= l
	if n < 0 { // Convert left shift to equivalent right shift.
		return n + l
	}
	return n
}
