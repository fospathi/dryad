package eqnode

import (
	"gitlab.com/fospathi/dryad"
)

// ContentSet contains the contents of the nodes for the tree with the given
// root node.
func ContentSet[T comparable](root *dryad.NodeOfEq[T]) dryad.Set[T] {
	cs := dryad.Set[T]{}
	Traverse(root, func(node *dryad.NodeOfEq[T]) {
		cs.Add(node.Content)
	})
	return cs
}

// CopyTree creates a copy of the tree with the given root node.
func CopyTree[T comparable](root *dryad.NodeOfEq[T]) *dryad.NodeOfEq[T] {
	rootCpy := &dryad.NodeOfEq[T]{}
	var traverse func(n1, n2 *dryad.NodeOfEq[T])
	traverse = func(n1, n2 *dryad.NodeOfEq[T]) {
		n2.Content = n1.Content
		for _, k := range n1.Kids {
			kCpy := &dryad.NodeOfEq[T]{}
			n2.AddChild(kCpy)
			traverse(k, kCpy)
		}
	}
	traverse(root, rootCpy)
	return rootCpy
}

// LeafSet contains the contents of the leaf nodes for the tree with the given
// root node.
func LeafSet[T comparable](root *dryad.NodeOfEq[T]) dryad.Set[T] {
	ls := dryad.Set[T]{}
	for _, v := range Leaves(root) {
		ls.Add(v.Content)
	}
	return ls
}

// LeafSlice contains the contents of the leaf nodes for the tree with the given
// root node.
func LeafSlice[T comparable](root *dryad.NodeOfEq[T]) []T {
	var vv []T
	Traverse(root, func(node *dryad.NodeOfEq[T]) {
		if len(node.Kids) == 0 {
			vv = append(vv, node.Content)
		}
	})
	return vv
}

// Leaves of the tree with the given root node.
func Leaves[T comparable](root *dryad.NodeOfEq[T]) []*dryad.NodeOfEq[T] {
	var vl []*dryad.NodeOfEq[T]
	Traverse(root, func(node *dryad.NodeOfEq[T]) {
		if len(node.Kids) == 0 {
			vl = append(vl, node)
		}
	})
	return vl
}

// Shed the leaf nodes from the tree with the given root node.
func Shed[T comparable](root *dryad.NodeOfEq[T]) {
	vl := Leaves(root)
	for _, l := range vl {
		l.Detach()
	}
}

// Traverse the tree with the given root node in a pre-order fashion applying
// the given function to each node.
func Traverse[T comparable](
	root *dryad.NodeOfEq[T],
	f func(*dryad.NodeOfEq[T]),
) {

	f(root)
	for _, kid := range root.Kids {
		Traverse(kid, f)
	}
}
