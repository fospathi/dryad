package dryad_test

import (
	"testing"

	"slices"

	"gitlab.com/fospathi/dryad"
	"gitlab.com/fospathi/dryad/modslice"
)

func TestModSlice_CanonShift(t *testing.T) {
	v := dryad.ModSlice[int]{1, 2, 3, 4, 5, 6}
	l := len(v)

	// Test a right shift.

	v.CanonShift(2)
	want := []int{5, 6, 1, 2, 3, 4}
	got := v
	if slices.Compare(want, got) != 0 {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	// Test the reverse shift.

	v.CanonShift(modslice.CanonShift(l, -2))
	want = []int{1, 2, 3, 4, 5, 6}
	got = v
	if slices.Compare(want, got) != 0 {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	// Test another left shift.

	v.CanonShift(modslice.CanonShift(l, -2))
	want = []int{3, 4, 5, 6, 1, 2}
	got = v
	if slices.Compare(want, got) != 0 {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	// Test a right shift bigger than the slice.

	v.CanonShift(modslice.CanonShift(l, 14)) // Rightwards by 2: 14 % 6 == 2.
	want = []int{1, 2, 3, 4, 5, 6}
	got = v
	if slices.Compare(want, got) != 0 {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	// Test a zero shift.

	v.CanonShift(modslice.CanonShift(l, 6))
	want = []int{1, 2, 3, 4, 5, 6}
	got = v
	if slices.Compare(want, got) != 0 {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	// Shift an empty slice.

	v = dryad.ModSlice[int]{}
	l = len(v)
	v.CanonShift(modslice.CanonShift(l, 6))
	want = []int{}
	got = v
	if slices.Compare(want, got) != 0 {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestModSlice_Shift(t *testing.T) {
	v := dryad.ModSlice[int]{1, 2, 3, 4, 5, 6}

	// Test a right shift.

	v.Shift(2)
	want := []int{5, 6, 1, 2, 3, 4}
	got := v
	if slices.Compare(want, got) != 0 {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	// Test the reverse shift.

	v.Shift(-2)
	want = []int{1, 2, 3, 4, 5, 6}
	got = v
	if slices.Compare(want, got) != 0 {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	// Test another left shift.

	v.Shift(-2)
	want = []int{3, 4, 5, 6, 1, 2}
	got = v
	if slices.Compare(want, got) != 0 {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	// Test a right shift bigger than the slice.

	v.Shift(14) // Rightwards by 2: 14 % 6 == 2.
	want = []int{1, 2, 3, 4, 5, 6}
	got = v
	if slices.Compare(want, got) != 0 {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	// Test a zero shift.

	v.Shift(6)
	want = []int{1, 2, 3, 4, 5, 6}
	got = v
	if slices.Compare(want, got) != 0 {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	// Shift an empty slice.

	v = dryad.ModSlice[int]{}
	v.Shift(1)
	want = []int{}
	got = v
	if slices.Compare(want, got) != 0 {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	// Shift a single element slice.

	v = dryad.ModSlice[int]{1}
	v.Shift(1)
	want = []int{1}
	got = v
	if slices.Compare(want, got) != 0 {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestModSlice_ShiftByOne(t *testing.T) {
	v := dryad.ModSlice[int]{1, 2, 3, 4, 5, 6}

	// Test a right shift.

	v.ShiftByOne(true)
	want := []int{6, 1, 2, 3, 4, 5}
	got := v
	if slices.Compare(want, got) != 0 {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	// Test the reverse shift.

	v.ShiftByOne(false)
	want = []int{1, 2, 3, 4, 5, 6}
	got = v
	if slices.Compare(want, got) != 0 {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	// Shift an empty slice.

	v = dryad.ModSlice[int]{}
	v.ShiftByOne(true)
	want = []int{}
	got = v
	if slices.Compare(want, got) != 0 {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestModSlice_ShiftLeft(t *testing.T) {
	v := dryad.ModSlice[int]{1, 2, 3, 4, 5, 6}

	v.ShiftLeft()
	want := []int{2, 3, 4, 5, 6, 1}
	got := v
	if slices.Compare(want, got) != 0 {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	// Shift an empty slice.

	v = dryad.ModSlice[int]{}
	v.ShiftLeft()
	want = []int{}
	got = v
	if slices.Compare(want, got) != 0 {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestModSlice_ShiftRight(t *testing.T) {
	v := dryad.ModSlice[int]{1, 2, 3, 4, 5, 6}

	v.ShiftRight()
	want := []int{6, 1, 2, 3, 4, 5}
	got := v
	if slices.Compare(want, got) != 0 {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	// Shift an empty slice.

	v = dryad.ModSlice[int]{}
	v.ShiftRight()
	want = []int{}
	got = v
	if slices.Compare(want, got) != 0 {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}
