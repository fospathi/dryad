package dryad

// A Multiset of comparable elements.
type Multiset[T comparable] map[T]int

// Add the given element to the set.
func (s Multiset[T]) Add(v T) {
	n, ok := s[v]
	if !ok {
		s[v] = 1
		return
	}
	s[v] = n + 1
}

// Cardinality of the set.
func (s Multiset[T]) Cardinality() int {
	var n int
	for _, m := range s {
		n += m
	}
	return n
}

// Multiplicity of the given element.
func (s Multiset[T]) Multiplicity(v T) int {
	n := s[v]
	return n
}
