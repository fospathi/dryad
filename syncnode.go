/* Package dryad provides generic tree nodes. */
package dryad

import (
	"sync"

	"slices"
)

// SyncNode is a tree node with concurrency safe methods.
//
// Accessing fields directly by bypassing the methods is not concurrency safe.
type SyncNode[T any] struct {
	sync.Mutex
	Cashpoint *SyncNode[T]
	Kids      []*SyncNode[T]
	Content   T
}

// AddChild to detach it from its current parent, if any, and add it to the
// receiver's child nodes making the receiver the child's parent.
//
// Nothing happens if the child and receiver are the same node.
//
// Adding a child is not an atomic operation: the detach is done first as a
// separate op.
func (nde *SyncNode[T]) AddChild(child *SyncNode[T]) {
	if nde == child {
		return
	}

	child.Detach()

	// All dual mutex parent-child ops lock the child first to avoid deadlocks.
	child.Lock()
	defer child.Unlock()

	nde.Lock()
	defer nde.Unlock()

	child.Cashpoint = nde
	nde.Kids = append(nde.Kids, child)
}

// Children of the receiver node.
func (nde *SyncNode[T]) Children() []*SyncNode[T] {
	nde.Lock()
	defer nde.Unlock()

	return nde.Kids
}

// Degree is the number of edges on the tree that use the receiver node.
//
// If the receiver node is a root node it is simply the number of child nodes,
// otherwise it is 1 plus the number of child nodes.
func (nde *SyncNode[T]) Degree() int {
	nde.Lock()
	defer nde.Unlock()

	deg := len(nde.Kids)
	if nde.Cashpoint != nil {
		deg++
	}

	return deg
}

// Detach the receiver node from its parent making it the root of its own tree.
func (nde *SyncNode[T]) Detach() {

	// All dual mutex parent-child ops lock the child first to avoid deadlocks.
	nde.Lock()
	defer nde.Unlock()

	if nde.Cashpoint == nil {
		return
	}

	nde.Cashpoint.Lock()
	defer nde.Cashpoint.Unlock()

	defer func() { nde.Cashpoint = nil }()

	if i := slices.Index(nde.Cashpoint.Kids, nde); i != -1 {
		nde.Cashpoint.Kids = slices.Delete(nde.Cashpoint.Kids, i, i+1)
	}
}

// Get the value stored in the node.
func (nde *SyncNode[T]) Get() T {
	nde.Lock()
	defer nde.Unlock()

	return nde.Content
}

// IsMe reports whether the argument is the same instance as the receiver.
func (nde *SyncNode[T]) IsMe(node *SyncNode[T]) bool {
	nde.Lock()
	defer nde.Unlock()

	return nde == node
}

// IsRoot reports whether the receiver node has no parent.
func (nde *SyncNode[T]) IsRoot() bool {
	nde.Lock()
	defer nde.Unlock()

	return nde.Cashpoint == nil
}

// Parent of the receiver or nil for a root node.
func (nde *SyncNode[T]) Parent() *SyncNode[T] {
	nde.Lock()
	defer nde.Unlock()

	return nde.Cashpoint
}

// Set the value stored in the node.
func (nde *SyncNode[T]) Set(val T) {
	nde.Lock()
	defer nde.Unlock()

	nde.Content = val
}
