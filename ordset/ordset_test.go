package ordset_test

import (
	"testing"

	"slices"

	"gitlab.com/fospathi/dryad/eqset"
	"gitlab.com/fospathi/dryad/ordset"
)

func TestSorted(t *testing.T) {
	set := eqset.New("Annie", "Tammy", "Gizmo", "Tomm", "Coal")

	vv := ordset.Sorted(set)

	want, got := true, slices.Equal(
		vv,
		[]string{"Annie", "Coal", "Gizmo", "Tammy", "Tomm"},
	)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}
