package ordset

import (
	"slices"

	"gitlab.com/fospathi/dryad"
	"golang.org/x/exp/constraints"
)

// Sorted slice containing the elements of the set in ascending order.
func Sorted[T constraints.Ordered](s dryad.Set[T]) []T {
	vv := make([]T, 0, len(s))
	for v := range s {
		vv = append(vv, v)
	}
	slices.Sort(vv)
	return vv
}
