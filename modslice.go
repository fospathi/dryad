package dryad

import "slices"

// ModSlice uses modular arithmetic indices.
type ModSlice[T any] []T

// CanonShift the slice values rightwards by n positions where n is a canonical
// shift value, that is, positive and less than the slice length.
//
// A value pushed off the right (highest index) edge is inserted at the left
// edge.
func (vv ModSlice[T]) CanonShift(n int) {
	temp := make([]T, n)
	copy(temp, vv[len(vv)-n:])
	copy(vv[n:], vv[:])
	copy(vv[:], temp)
}

// Shift the slice values by n positions.
//
// It is acceptable if the given number of positions to shift the values by is
// negative or larger in magnitude than the slice length. Positive shifts move
// the values right towards the highest index. A value pushed off an edge is
// inserted at the other edge.
func (vv ModSlice[T]) Shift(n int) {
	l := len(vv)
	if l <= 1 {
		return
	}
	n %= l
	if n == 0 {
		return
	} else if n < 0 { // Convert left shift to equivalent right shift.
		n += l
	}
	temp := make([]T, n)
	copy(temp, vv[l-n:])
	copy(vv[n:], vv[:])
	copy(vv[:], temp)
}

// ShiftByOne position.
//
// A true value shifts values right towards the highest slice index.
func (vv ModSlice[T]) ShiftByOne(right bool) {
	l := len(vv)
	if l <= 1 {
		return
	} else if right {
		temp := vv[l-1]
		copy(vv[1:], vv[:])
		vv[0] = temp

	} else {
		temp := vv[0]
		copy(vv[:], vv[1:])
		vv[l-1] = temp
	}
}

// ShiftLeft by one position towards slice index 0.
//
// The first slice value is copied to the last position.
func (vv ModSlice[T]) ShiftLeft() {
	l := len(vv)
	if l <= 1 {
		return
	}
	temp := vv[0]
	copy(vv[:], vv[1:])
	vv[l-1] = temp
}

// ShiftRight by one position towards the highest slice index.
//
// The last slice value is copied to the first position.
func (vv ModSlice[T]) ShiftRight() {
	l := len(vv)
	if l <= 1 {
		return
	}
	temp := vv[l-1]
	copy(vv[1:], vv[:])
	vv[0] = temp
}

// ModSliceOfEq uses modular arithmetic indices.
type ModSliceOfEq[T comparable] ModSlice[T]

// StartWith the given value by shifting the slice values so the given value's
// first occurrence occupies index 0.
func (vv ModSliceOfEq[T]) StartWith(v T) {
	if n := slices.Index(vv, v); n > 0 {
		ModSlice[T](vv).Shift(-n)
	}
}

// StartWithFunc shifts the slice values so that the given functions first true
// match occupies index 0.
func (vv ModSliceOfEq[T]) StartWithFunc(f func(v T) bool) {
	if n := slices.IndexFunc(vv, f); n > 0 {
		ModSlice[T](vv).Shift(-n)
	}
}
