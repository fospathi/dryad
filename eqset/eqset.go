package eqset

import (
	"gitlab.com/fospathi/dryad"
	"golang.org/x/exp/constraints"
)

// FromKeys constructs a new set using the keys of a map.
func FromKeys[K comparable, V any](m map[K]V) dryad.Set[K] {
	s := dryad.Set[K]{}
	for k := range m {
		s[k] = struct{}{}
	}
	return s
}

// MapToOrd uses the given function to map the elements of the given set of
// comparable elements to a newly allocated set of ordered type elements.
func MapToOrd[T comparable, S constraints.Ordered](
	src dryad.Set[T],
	f func(T) S,
) dryad.Set[S] {

	dest := dryad.Set[S]{}
	for v := range src {
		dest.Add(f(v))
	}
	return dest
}

// New constructs a new set.
func New[T comparable](vv ...T) dryad.Set[T] {
	s := dryad.Set[T]{}
	s.AddAll(vv...)
	return s
}

// ToMap uses the set's elements as keys in a newly allocated map.
//
// The given value is assigned as the initial value of every item in the map.
func ToMap[K comparable, V any](set dryad.Set[K], v V) map[K]V {
	m := map[K]V{}
	for k := range set {
		m[k] = v
	}
	return m
}
