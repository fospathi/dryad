package leafstar_test

import (
	"strings"
	"sync"
	"testing"

	"slices"

	"gitlab.com/fospathi/dryad"
	"gitlab.com/fospathi/dryad/leafstar"
)

func TestTraverse(t *testing.T) {

	nodeA := &dryad.SyncNode[string]{}
	nodeB := &dryad.SyncNode[string]{}
	nodeC := &dryad.SyncNode[string]{}
	nodeD := &dryad.SyncNode[string]{}

	nodeA.Set("A")
	nodeB.Set("B")
	nodeC.Set("C")
	nodeD.Set("D")

	m := &sync.Mutex{}
	externalAcc := []string{}
	var f leafstar.F[*dryad.SyncNode[string], string, []string]
	f = func(
		node *dryad.SyncNode[string],
		step leafstar.PreviousStep,
		_ []string,
	) []string {

		m.Lock()
		defer m.Unlock()

		externalAcc = append(externalAcc, node.Get())

		return nil
	}

	// Test a single node tree leafstar traversal.

	leafstar.Traverse(nodeA, nil, f)

	{
		want := true
		got := slices.Contains(externalAcc, "A") &&
			len(externalAcc) == 1
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	// Test a multinode leafstar traversal.

	externalAcc = []string{}

	nodeA.AddChild(nodeB)
	nodeA.AddChild(nodeC)
	nodeC.AddChild(nodeD)
	//           A
	//          / \
	//         B   C
	//              \
	//               D

	leafstar.Traverse(nodeA, nil, f)

	{
		want := true
		got := slices.Contains(externalAcc, "A") &&
			slices.Contains(externalAcc, "B") &&
			slices.Contains(externalAcc, "C") &&
			slices.Contains(externalAcc, "D") &&
			len(externalAcc) == 4
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	// Test starting a traversal from a non root node.

	externalAcc = []string{}

	leafstar.Traverse(nodeC, nil, f)

	{
		want := true
		got := slices.Contains(externalAcc, "A") &&
			slices.Contains(externalAcc, "B") &&
			slices.Contains(externalAcc, "C") &&
			slices.Contains(externalAcc, "D") &&
			len(externalAcc) == 4
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	// Test accumulator passing.

	externalAcc = []string{}

	f = func(
		node *dryad.SyncNode[string],
		step leafstar.PreviousStep,
		acc []string,
	) []string {

		acc = slices.Clone(acc)
		acc = append(acc, node.Get())

		if len(node.Children()) == 0 {
			m.Lock()
			defer m.Unlock()

			// Record accumulated path history at leaf nodes.
			externalAcc = append(externalAcc, strings.Join(acc, " "))
		}

		return acc
	}

	leafstar.Traverse(nodeC, nil, f)

	{
		want := true
		got := slices.Contains(externalAcc, "C D") &&
			slices.Contains(externalAcc, "C A B") &&
			len(externalAcc) == 2
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	// Test starting a traversal from a leaf.

	externalAcc = []string{}

	leafstar.Traverse(nodeD, nil, f)

	{
		want := true
		got := slices.Contains(externalAcc, "D") &&
			slices.Contains(externalAcc, "D C A B") &&
			len(externalAcc) == 2
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	// Test a more complex tree.

	nodeE := &dryad.SyncNode[string]{}
	nodeF := &dryad.SyncNode[string]{}

	nodeE.Set("E")
	nodeF.Set("F")

	nodeC.AddChild(nodeE)
	nodeC.AddChild(nodeF)
	//           A
	//          / \
	//         B   C
	//            /|\
	//           / | \
	//          D  E  F

	externalAcc = []string{}

	leafstar.Traverse(nodeD, nil, f)

	{
		want := true
		got := slices.Contains(externalAcc, "D") &&
			slices.Contains(externalAcc, "D C E") &&
			slices.Contains(externalAcc, "D C F") &&
			slices.Contains(externalAcc, "D C A B") &&
			len(externalAcc) == 4
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}
}
