// Package leafstar implements a tree traversal that fans out from a given node
// to the tree's leaf nodes. Upwards steps from a child to parent are allowed.
package leafstar

import (
	"sync"
)

// A Node on a tree of nodes.
type Node[T, C any] interface {
	IsMe(T) bool
	IsRoot() bool

	Children() []T
	Parent() T

	Get() C
	Set(C)
}

// F is applied once to each node during a leafstar traversal.
//
// The node argument is the current node, the previous step argument shows the
// way the current node was reached, and the accumulator argument is the either
// the initial accumulator value for the start node or the return value of the
// previous application of this same function to the previous node on the path.
type F[T Node[T, C], C, A any] func(T, PreviousStep, A) A

// Traverse the tree in a concurrent leafstar fashion starting at the given node
// with the given initial accumulator value.
//
// For a tree with n leaf nodes, and allowing upwards steps, there are n
// possible paths from any given node to each leaf. A leafstar traversal spreads
// out concurrently from the start node following n possible paths to n leaf
// nodes. Each node is visited only once even for nodes in overlapping paths.
//
// Return when the given function has been applied to every node.
func Traverse[T Node[T, C], C, A any](start T, acc A, f F[T, C, A]) {
	traverse(
		stepArg[T, C, A]{
			start, acc, Start, *new(T), f, &sync.WaitGroup{},
		},
	)
}

// PreviousStep shows how a node was arrived at by specifying the immediately
// preceding step in the path; either a type of edge traversal from an
// immediately connected node or no step at all for the start node.
type PreviousStep uint

const (
	// This is the start node of the paths. There is no previous edge traversal.
	Start PreviousStep = iota

	// Traced a step in the path towards a leaf node going down the tree from a
	// parent to a child node.
	Down
	// Traced a step in the path towards a leaf node going up the tree from a
	// child to a parent node.
	Up
)

func traverse[T Node[T, C], C, A any](arg stepArg[T, C, A]) {

	kids, cashpoint, unvisited := edges[T, C](arg.n)
	if arg.prevStep != Start {
		unvisited-- // Adjacent unvisited nodes - discount the previous node.
	}
	arg.wg.Add(unvisited)
	acc := arg.f(arg.n, arg.prevStep, arg.acc)

	switch arg.prevStep {

	case Start:

		if !arg.n.IsRoot() {
			go func(arg stepArg[T, C, A]) {
				traverse(arg)
			}(arg.nextStepArg(cashpoint, acc, Up, arg.n))
		}

		for _, kid := range kids {
			go func(arg stepArg[T, C, A]) {
				traverse(arg)
			}(arg.nextStepArg(kid, acc, Down, *new(T)))
		}

		arg.wg.Wait() // Block until all goroutines finish.

	case Down:

		for _, kid := range kids {
			go func(arg stepArg[T, C, A]) {
				traverse(arg)
			}(arg.nextStepArg(kid, acc, Down, *new(T)))
		}

		arg.wg.Done()

	case Up:

		if !arg.n.IsRoot() {
			go func(arg stepArg[T, C, A]) {
				traverse(arg)
			}(arg.nextStepArg(cashpoint, acc, Up, arg.n))
		}

		for _, kid := range kids {
			if kid.IsMe(arg.srcKid) {
				continue
			}
			go func(arg stepArg[T, C, A]) {
				traverse(arg)
			}(arg.nextStepArg(kid, acc, Down, *new(T)))
		}

		arg.wg.Done()
	}
}

func edges[T Node[T, C], C any](n Node[T, C]) ([]T, T, int) {
	kids := n.Children()
	cashpoint := n.Parent() // cashpoint may be nil
	count := len(kids)
	if !n.IsRoot() {
		count++
	}
	return kids, cashpoint, count
}

type stepArg[T Node[T, C], C, A any] struct {
	n        T // The current node.
	acc      A
	prevStep PreviousStep
	srcKid   T // Unused for a down step or start step.

	f  F[T, C, A]
	wg *sync.WaitGroup
}

func (arg stepArg[T, C, A]) nextStepArg(
	dest T,
	acc A,
	prevStep PreviousStep,
	srcKid T,
) stepArg[T, C, A] {
	return stepArg[T, C, A]{
		n:        dest,
		acc:      acc,
		prevStep: prevStep,
		srcKid:   srcKid,

		f:  arg.f,
		wg: arg.wg,
	}
}
