package dryad_test

import (
	"fmt"
	"sync"
	"testing"

	"slices"

	"gitlab.com/fospathi/dryad"
)

func TestSyncAwaitSeq_Advance(t *testing.T) {
	{
		var (
			seq = dryad.NewSyncAwaitSeq[int]()

			dest  []int
			destM = sync.Mutex{}

			done = make(chan struct{})
		)

		// Start a loop and use advance to read any values added to the sequence
		// and add them to dest. The loop signifies it has detected closure of
		// the sequence by adding another value to dest which was never added to
		// the sequence.
		//
		// The loop is done when the sequence is closed and there are no unread
		// values.
		go func() {
			var (
				err     error
				prev, w dryad.Await
				v       int
			)
			defer close(done)

			for {
				w, _ = seq.Advance(prev)
				<-w
				v, err = seq.Value(w)

				select {
				case <-seq.Done:
					if err == nil {
						break
					}
					destM.Lock()
					dest = append(dest, 9)
					destM.Unlock()
					return
				default:
				}

				destM.Lock()
				dest = append(dest, v)
				destM.Unlock()
				prev = w
			}
		}()

		// Add some values to the sequence and then close it.
		seq.Append(5)
		seq.Append(6)
		seq.Append(7)
		seq.Append(8)
		seq.Close()

		<-done

		// Test that the values were extracted from the sequence.

		destM.Lock()
		want := []int{5, 6, 7, 8, 9}
		got := dest
		if !slices.Equal(want, got) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
		destM.Unlock()
	}

	{
		var (
			seq = dryad.NewSyncAwaitSeq[int]()

			dest  []int
			destM = sync.Mutex{}

			done = make(chan struct{})
		)

		seq.Append(-1)
		seq.Append(-2)
		seq.Close()

		// This loop reads values from the sequence until it gets an unfilled
		// position error.
		go func() {
			var (
				err     error
				prev, w dryad.Await
				v       int
			)
			defer close(done)

		forLoop:
			for {
				w, _ = seq.Advance(prev)
				<-w
				v, err = seq.Value(w)
				if dryad.IsUnfilledError(err) {
					break forLoop
				}
				destM.Lock()
				dest = append(dest, v)
				destM.Unlock()
				prev = w
			}
		}()

		<-done

		// Test that the values were extracted from the sequence.

		destM.Lock()
		want := []int{-1, -2}
		got := dest
		if !slices.Equal(want, got) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
		destM.Unlock()
	}
}

func TestSyncAwaitSeq_Append(t *testing.T) {
	seq := dryad.NewSyncAwaitSeq[int]()

	for i := 0; i < 4; i++ {
		seq.Append(i)
	}

	vv := make([]int, 4)
	for i, v := range seq.Values() {
		vv[i] = v
	}

	want := true
	got := slices.Equal(vv, []int{0, 1, 2, 3})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	seq.Close()
	seq.Append(1)
	{
		want := 4
		got := len(seq.Values())
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}
}

func TestSyncAwaitSeq_AppendAll(t *testing.T) {
	seq := dryad.NewSyncAwaitSeq[int]()
	seq.AppendAll([]int{-1, -2})

	want := 2
	got := len(seq.Values())
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	waiter, _ := seq.Advance(nil)
	v, _ := seq.Value(waiter)
	want = -1
	got = v
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	waiter, _ = seq.Advance(waiter)
	v, _ = seq.Value(waiter)
	want = -2
	got = v
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	waiter, _ = seq.Advance(waiter)
	_, err := seq.Value(waiter)
	{
		want := true
		got := dryad.IsUnfilledError(err)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	seq.AppendAll([]int{-3})
	v, _ = seq.Value(waiter)
	want = -3
	got = v
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	seq.Close()
	seq.AppendAll([]int{-4, -5})
	want = 3
	got = len(seq.Values())
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestSyncAwaitSeq_Close(t *testing.T) {
	seq := dryad.NewSyncAwaitSeq[int]()
	seq.Append(5)
	// Test multiple closes.
	seq.Close()
	seq.Close()
	seq.Append(7)

	want := 1
	got := len(seq.Values())
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	waiter, _ := seq.Advance(nil)
	v, _ := seq.Value(waiter)
	want = 5
	got = v
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestSyncAwaitSeq_Delete(t *testing.T) {
	seq := dryad.NewSyncAwaitSeq[int]()

	waiter, _ := seq.Advance(nil)
	_, err := seq.Delete(waiter)
	{
		want := true
		got := dryad.IsUnfilledError(err)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	_, err = seq.Delete(nil)
	{
		want := true
		got := dryad.IsAwaitNotFoundError(err)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	seq.AppendAll([]int{-1})
	v, _ := seq.Delete(waiter)
	want := -1
	got := v
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = 0
	got = len(seq.Values())
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	seq.AppendAll([]int{-2, -3})

	_, err = seq.Delete(waiter)
	{
		want := true
		got := dryad.IsAwaitNotFoundError(err)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	w1, _ := seq.Advance(nil)
	w2, _ := seq.Advance(w1)
	v1, _ := seq.Delete(w1)
	v2, _ := seq.Delete(w2)
	want1, want2 := -2, -3
	got1, got2 := v1, v2
	if want1 != got1 || want2 != got2 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n",
			want1, got1, want2, got2)
	}

	want = 0
	got = len(seq.Values())
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestIsAwaitNotFoundError(t *testing.T) {
	seq := dryad.NewSyncAwaitSeq[int]()

	_, err := seq.Value(nil)
	want := true
	got := dryad.IsAwaitNotFoundError(err)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	_, err = seq.Advance(make(<-chan struct{}))
	want = true
	got = dryad.IsAwaitNotFoundError(err)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	seq.Append(1)

	_, err = seq.Advance(make(<-chan struct{}))
	want = true
	got = dryad.IsAwaitNotFoundError(err)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	// Test wrapping the error.

	wErr := fmt.Errorf("wrapped error: %w", err)
	got = dryad.IsAwaitNotFoundError(wErr)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestIsUnfilledError(t *testing.T) {
	seq := dryad.NewSyncAwaitSeq[int]()

	waiter, _ := seq.Advance(nil)
	_, err := seq.Advance(waiter)

	want := true
	got := dryad.IsUnfilledError(err)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	_, err = seq.Value(waiter)
	got = dryad.IsUnfilledError(err)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	seq.Append(1)

	waiter, _ = seq.Advance(waiter)
	_, err = seq.Value(waiter)
	got = dryad.IsUnfilledError(err)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	// Test wrapping the error.

	wErr := fmt.Errorf("wrapped error: %w", err)
	got = dryad.IsUnfilledError(wErr)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}
