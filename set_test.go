package dryad_test

import (
	"cmp"
	"testing"

	"slices"

	"gitlab.com/fospathi/dryad"
	"gitlab.com/fospathi/dryad/eqset"
)

func TestSet(t *testing.T) {
	var (
		empty    = dryad.Set[string]{}
		nonEmpty = eqset.New("Annie", "Tammy", "Gizmo")
	)

	{ // Element quantity.
		want, got := true,
			empty.IsEmpty() &&
				!empty.IsNotEmpty() &&
				empty.Len() == 0
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}

		want, got = true,
			!nonEmpty.IsEmpty() &&
				nonEmpty.IsNotEmpty() &&
				nonEmpty.Len() == len(nonEmpty)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	{ // Adding, deleting, and copying.
		empty.Add("Annie")
		empty.AddAll("Tammy", "Gizmo")
		want, got := true, empty.Equals(nonEmpty)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}

		copy := empty.Copy()

		empty.Delete("Annie")
		empty.DeleteAll("Tammy", "Gizmo")
		want, got = true, empty.IsEmpty()
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}

		want, got = true, copy.Equals(nonEmpty)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}

		empty.AddSet(nonEmpty)
		want, got = true, empty.Equals(nonEmpty)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}
}

func TestSet_Contains(t *testing.T) {
	var (
		empty        = dryad.Set[string]{}
		set          = eqset.New("Annie", "Tammy")
		subset       = eqset.New("Annie")
		intersecting = eqset.New("Annie", "Gizmo")
	)

	want := true

	got := set.Contains("Annie")
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	got = set.ContainsAll(subset.Elements()...)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	got = !set.ContainsAll(empty.Elements()...)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	got = !set.ContainsAll(intersecting.Elements()...)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestSet_Difference(t *testing.T) {
	var (
		empty        = dryad.Set[string]{}
		intersecting = eqset.New("Annie", "Gizmo")
		set          = eqset.New("Annie", "Tammy")
		subset       = eqset.New("Annie")
		union        = eqset.New("Annie", "Gizmo", "Tammy")
	)

	want := true

	got := set.Difference(subset).Equals(eqset.New("Tammy"))
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	got = subset.Difference(set).Equals(empty)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	got = union.Difference(intersecting).Equals(eqset.New("Tammy"))
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestSet_Equals(t *testing.T) {
	var (
		empty        = dryad.Set[string]{}
		intersecting = eqset.New("Annie", "Gizmo")
		set          = eqset.New("Annie", "Tammy")
		subset       = eqset.New("Annie")
		union        = eqset.New("Annie", "Gizmo", "Tammy")
	)

	want := true

	got := !set.Equals(empty)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	got = empty.Equals(empty)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	got = !empty.Equals(set)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	got = !set.Equals(intersecting)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	got = !set.Equals(subset)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	got = set.Union(intersecting).Equals(union)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestSet_Intersection(t *testing.T) {
	var (
		empty        = dryad.Set[string]{}
		set          = eqset.New("Annie", "Tammy")
		subset       = eqset.New("Annie")
		intersecting = eqset.New("Annie", "Gizmo", "Tomm")
	)

	want := true

	got := set.Intersection(empty).Equals(empty)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	got = set.Intersection(intersecting).Equals(subset)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	got = intersecting.Intersection(set).Equals(subset)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	got = intersecting.Intersects(set)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	got = !empty.Intersects(set)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestSet_IsSupersetOf(t *testing.T) {
	var (
		empty        = dryad.Set[string]{}
		set          = eqset.New("Annie", "Tammy")
		subset       = eqset.New("Annie")
		intersecting = eqset.New("Annie", "Gizmo")
	)

	want := true

	got := set.IsSupersetOf(empty)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	got = !empty.IsSupersetOf(set)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	got = set.IsSupersetOf(subset)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	got = !set.IsSupersetOf(intersecting)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestSet_Sorted(t *testing.T) {
	set := eqset.New("Annie", "Tammy", "Gizmo", "Tomm", "Coal")

	want, got := true, slices.Equal(
		set.Sorted(cmp.Compare),
		[]string{"Annie", "Coal", "Gizmo", "Tammy", "Tomm"},
	)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestSet_Union(t *testing.T) {
	var (
		empty        = dryad.Set[string]{}
		set          = eqset.New("Annie", "Tammy")
		intersecting = eqset.New("Annie", "Gizmo")
		union        = eqset.New("Annie", "Gizmo", "Tammy")
	)

	want := true

	got := set.Union(empty).Equals(set)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	got = set.Union(intersecting).Equals(union)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}
