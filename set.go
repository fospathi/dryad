package dryad

import (
	"slices"
)

// A Set of comparable elements.
type Set[T comparable] map[T]struct{}

// Add the given element to the set.
func (s Set[T]) Add(v T) {
	s[v] = struct{}{}
}

// AddAll the given elements to the set.
func (s Set[T]) AddAll(vv ...T) {
	for _, v := range vv {
		s[v] = struct{}{}
	}
}

// AddSet to the receiver.
//
// The receiver set becomes the union of both sets.
func (s Set[T]) AddSet(set Set[T]) {
	for v := range set {
		s.Add(v)
	}
}

// Any pseudo random element of the non empty set.
func (s Set[T]) Any() T {
	var zero T
	for v := range s {
		return v
	}
	return zero
}

// Contains the given element if true.
func (s Set[T]) Contains(v T) bool {
	_, ok := s[v]
	return ok
}

// ContainsAll the elements of the given non empty slice if true.
//
// Special cases:
//   - return false if the given slice is empty. See also [.IsSuperset] for
//     alternative behaviour for the empty set case.
func (s Set[T]) ContainsAll(vv ...T) bool {
	for _, v := range vv {
		if _, ok := s[v]; !ok {
			return false
		}
	}
	return len(vv) != 0
}

// Copy returns a newly allocated set containing the same elements.
func (s Set[T]) Copy() Set[T] {
	set := Set[T]{}
	for v := range s {
		set.Add(v)
	}
	return set
}

// Delete the given element from the set.
func (s Set[T]) Delete(v T) {
	delete(s, v)
}

// DeleteAll the given elements from the set.
func (s Set[T]) DeleteAll(vv ...T) {
	for _, v := range vv {
		delete(s, v)
	}
}

// Difference set containing those elements of the receiver set which are absent
// from the argument set.
func (s Set[T]) Difference(set Set[T]) Set[T] {
	dif := Set[T]{}
	for v := range s {
		if !set.Contains(v) {
			dif.Add(v)
		}
	}
	return dif
}

// Elements of the set as a newly allocated slice.
func (s Set[T]) Elements() []T {
	vv := make([]T, 0, len(s))
	for v := range s {
		vv = append(vv, v)
	}
	return vv
}

// Equals reports whether the sets are equal.
func (s Set[T]) Equals(set Set[T]) bool {
	if len(s) != len(set) {
		return false
	}
	for v := range s {
		if !set.Contains(v) {
			return false
		}
	}
	return true
}

// Intersection of the sets containing the elements shared by both sets.
func (s Set[T]) Intersection(set Set[T]) Set[T] {
	var (
		shared = Set[T]{}
		s1, s2 Set[T]
	)
	if s.Len() < set.Len() {
		s1, s2 = s, set
	} else {
		s1, s2 = set, s
	}
	for v := range s1 {
		if s2.Contains(v) {
			shared[v] = struct{}{}
		}
	}
	return shared
}

// Intersects reports whether the sets share any elements.
func (s Set[T]) Intersects(set Set[T]) bool {
	var s1, s2 Set[T]
	if len(s) < len(set) {
		s1, s2 = s, set
	} else {
		s1, s2 = set, s
	}
	for v := range s1 {
		if s2.Contains(v) {
			return true
		}
	}
	return false
}

// IsEmpty reports whether the set has no elements.
func (s Set[T]) IsEmpty() bool {
	return len(s) == 0
}

// IsNotEmpty reports whether the set has at least one element.
func (s Set[T]) IsNotEmpty() bool {
	return len(s) != 0
}

// IsSupersetOf reports whether the receiver set is a superset of the given set.
//
// Special cases:
//   - return true if the given set is empty. See also [.ContainsAll] for
//     alternative behaviour for the empty set case.
func (s Set[T]) IsSupersetOf(set Set[T]) bool {
	for v := range set {
		if _, ok := s[v]; !ok {
			return false
		}
	}
	return true
}

// Len is the number of elements in the set.
func (s Set[T]) Len() int {
	return len(s)
}

// Sorted elements of the set in ascending order as determined by the less
// function.
func (s Set[T]) Sorted(less func(T, T) int) []T {
	vv := s.Elements()
	slices.SortFunc(vv, less)
	return vv
}

// Union set containing all elements from both of the sets.
func (s Set[T]) Union(set Set[T]) Set[T] {
	u := Set[T]{}
	for v := range s {
		u[v] = struct{}{}
	}
	for v := range set {
		u[v] = struct{}{}
	}
	return u
}
